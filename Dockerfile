FROM ubuntu:latest
MAINTAINER Artur Bankowski <artur.bankowski@gmail.com>

RUN apt-get update && \
    apt-get install -y software-properties-common apt-transport-https wcstools

RUN add-apt-repository "deb http://http.debian.net/debian jessie-backports main" && \
    add-apt-repository "deb https://dl.bintray.com/sbt/debian /" && \
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823

RUN apt-get update && \
    apt-get install -y openjdk-8-jdk-headless sbt lintian fakeroot dpkg-dev dpkg-sig

RUN sbt sbt-version

COPY seed /tmp/seed

WORKDIR /tmp/seed

RUN sbt update

WORKDIR /root
