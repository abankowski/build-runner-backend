name := "dummy"

organization := "scalability"

version := "0.1-SNAPSHOT"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.webjars" % "swagger-ui" % "2.2.5",
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test,
  "com.github.nscala-time" %% "nscala-time" % "2.14.0",
  specs2 % Test
)
